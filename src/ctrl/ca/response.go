/*
Package ca comment
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/
package ca

import (
	"github.com/emirpasic/gods/lists/arraylist"

	"management_backend/src/db/chain_participant"
	dbcommon "management_backend/src/db/common"
)

// 这里的用来区分请求
const (
	CERT_FOR_SIGN = iota
	KEY_FOR_SIGN
	CERT_FOR_TLS
	KEY_FOR_TLS
	PEM_FOR_PUBLIC
	KEY_FOR_PUBLIC
)

const (
	// SIGNUSE sign
	SIGNUSE = iota
	// TLSUSE tls
	TLSUSE
)

// CertDetailView cert detail view
type CertDetailView struct {
	CertDetail string
}

// NewCertDetailView  new cert detail view
func NewCertDetailView(certDetail string) *CertDetailView {

	certDetailView := CertDetailView{
		CertDetail: certDetail,
	}

	return &certDetailView
}

// PkDetailView pk detail view
type PkDetailView struct {
	PublicKey  string
	PrivateKey string
}

// NewPkDetailView new pk detail view
func NewPkDetailView(publicKey, privateKey string) *PkDetailView {
	pkDetailView := PkDetailView{
		PublicKey:  publicKey,
		PrivateKey: privateKey,
	}
	return &pkDetailView
}

// CertListView cert list view
type CertListView struct {
	Id         int64
	UserName   string
	OrgName    string
	NodeName   string
	CertUse    int
	CertType   int
	Algorithm  int
	CreateTime int64
	RemarkName string
	Addr       string
}

// NewCertListView new cert list view
func NewCertListView(certs []*dbcommon.Cert) []interface{} {
	certViews := arraylist.New()
	for _, cert := range certs {
		var keyUse int
		var certUse int

		if cert.CertUse == SIGNUSE {
			keyUse = KEY_FOR_SIGN
			certUse = CERT_FOR_SIGN
		} else if cert.CertUse == TLSUSE {
			keyUse = KEY_FOR_TLS
			certUse = CERT_FOR_TLS
		}
		certView := CertListView{
			Id:         cert.Id,
			UserName:   cert.CertUserName,
			OrgName:    cert.OrgName,
			NodeName:   cert.NodeName,
			CertUse:    certUse,
			Algorithm:  cert.Algorithm,
			CertType:   cert.CertType,
			CreateTime: cert.CreatedAt.Unix(),
		}

		keyView := CertListView{
			Id:         cert.Id,
			UserName:   cert.CertUserName,
			OrgName:    cert.OrgName,
			NodeName:   cert.NodeName,
			CertUse:    keyUse,
			Algorithm:  cert.Algorithm,
			CertType:   cert.CertType,
			CreateTime: cert.CreatedAt.Unix(),
		}
		certViews.Add(keyView)
		certViews.Add(certView)
	}

	return certViews.Values()
}

// NewPkCertListView new pk cert list view
func NewPkCertListView(certs []*dbcommon.Cert) []interface{} {
	certViews := arraylist.New()
	for _, cert := range certs {
		var certUse int

		if cert.CertUse == PEM_CERT {
			certUse = PEM_FOR_PUBLIC
		}
		if cert.CertType == chain_participant.ADMIN {
			cert.CertType = chain_participant.USER
		}
		if cert.CertType == chain_participant.CONSENSUS {
			cert.CertType = chain_participant.NODE
		}
		certView := CertListView{
			Id:         cert.Id,
			RemarkName: cert.RemarkName,
			Addr:       cert.Addr,
			CertUse:    certUse,
			CertType:   cert.CertType,
			CreateTime: cert.CreatedAt.Unix(),
			Algorithm:  cert.Algorithm,
		}
		certViews.Add(certView)
	}

	return certViews.Values()
}
